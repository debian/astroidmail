Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Astroid
Upstream-Contact: https://github.com/astroidmail/astroid/issues
Source: https://github.com/astroidmail/astroid
 git://github.com/astroidmail/astroid

Files: *
Copyright:
  2014  Gaute Hope <eg@gaute.vetsj.com>
License-Grant:
 This program is free software:
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation,
 either version 3 of the License,
 or (at your option) any later version.
License: GPL-3+
Reference: LICENSE.md
Comment:
 Copyright and licensing information missing from source files.
 Copyright and license assumed from file <LICENSE.md>.

Files: ui/icons/*
Copyright:
  2017  Ura Design <https://ura.design>
License-Grant:
 This work is licensed as part of the Astroid project.
 You can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation,
 either version 3 of the License,
 or (at your option) any later version,
 or (at your option)
 under the Creative Commons Attribution 4.0 International License.
 To view a copy of this license,
 visit <http://creativecommons.org/licenses/by/4.0/>
 or send a letter to Creative Commons,
 PO Box 1866, Mountain View, CA 94042, USA.
License: GPL-3+
Reference: ui/icons/LICENSE
Comment:
 License grant alternatively permit CC-BY-4.0,
 ignored here as it is superfluous and bloats this copyright file.

Files: src/utils/gmime/*
Copyright:
  2016       Gaute Hope
  2000-2014  Jeffrey Stedfast
License-Grant:
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
License: LGPL-2.1+

Files:
 COPYING.GPL-3.0+
 COPYING.LGPL-2.1+
 ui/COPYING.LGPL-2.1+
Copyright: 1991, 1999, 2007, Free Software Foundation, Inc.
License: Changing-Not-Allowed

Files:
 cmake/GObjectIntrospectionMacros.cmake
 cmake/FindGObjectIntrospection.cmake
Copyright:
  2017  Alexander Adolf <alexander.adolf@condition-alpha.com>
  2010  Pino Toscano, <pino@kde.org>
License-Grant:
 Redistribution and use is allowed
 according to the terms of the BSD license.
 For details see the accompanying COPYING-CMAKE-SCRIPTS file.
License: BSD-3-Clause
Comment:
 Referenced file <COPYING-CMAKE-SCRIPTS> missing in source.
 License assumed from similarly named file in other projects.

Files:
 thread-view.css
 thread-view.html
Copyright:
  2012-2014  Yorba Foundation
License-Grant:
 This program is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License
 as published by the Free Software Foundation;
 either version 2.1 of the License,
 or (at your option) any later version.
License: LGPL-2.1+
Reference: LICENSE.md
Reference: ui/acknowledgement.md
Comment:
 Copyright and licensing information missing from source files.
 Copyright and license assumed from file <LICENSE.md>,
 via hints in file <ui/acknowledgement.md>.

Files: src/compose_message.cc
Copyright:
  2010  Michael Forney
License-Grant:
 This file is a part of ner.
 .
 ner is free software:
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License version 3,
 as published by the Free Software Foundation.
License: GPL-3
Reference: LICENSE.md
Comment:
 Copyright and licensing information missing from source file.
 Copyright and license assumed from file <LICENSE.md>.

Files: cmake/LibFindMacros.cmake
Copyright: None (in the Public Domain)
License: Public-Domain
 Public Domain, originally written by Lasse Kärkkäinen <tronic>

Files: .ycm_extra_conf.py
Copyright:
  Gaute Hope <eg@gaute.vetsj.com>
License: Unlicense
Comment:
 Copyright information missing from source file.
 Copyright assumed from commit author in git metadata.

Files: debian/*
Copyright:
  2016-2021, 2024  Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This packaging is free software:
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation,
 either version 3 of the License, or (at your option) any later version.
License: GPL-3+
Reference: debian/copyright

License: BSD-3-Clause
 Redistribution and use in source and binary forms,
 with or without modification, are permitted
 provided that the following conditions are met:
 .
  1. Redistributions of source code must retain
     the copyright notice,
      this list of conditions
     and the following disclaimer.
  2. Redistributions in binary form must reproduce
     the copyright notice,
     this list of conditions
     and the following disclaimer
     in the documentation and/or other materials
     provided with the distribution.
  3. The name of the author may not be used
     to endorse or promote products derived from this software
     without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS''
 AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3+
Reference: /usr/share/common-licenses/GPL-3

License: GPL-3
Reference: /usr/share/common-licenses/GPL-3

License: LGPL-2.1+
Reference: /usr/share/common-licenses/LGPL-2.1

License: Unlicense
 This is free and unencumbered software
 released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile,
 sell, or distribute this software,
 either in source code form or as a compiled binary,
 for any purpose, commercial or non-commercial,
 and by any means.
 .
 In jurisdictions that recognize copyright laws,
 the author or authors of this software dedicate
 any and all copyright interest in the software
 to the public domain.
 We make this dedication for the benefit of the public at large
 and to the detriment of our heirs and successors.
 We intend this dedication to be an overt act
 of relinquishment in perpetuity of all present and future rights
 to this software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 For more information, please refer to <http://unlicense.org/>

License: Changing-Not-Allowed
 Everyone is permitted to copy and distribute
 verbatim copies of this license document,
 but changing it is not allowed.
